package com.nelo.misiontic2022.ventasdomiciliog1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import com.nelo.misiontic2022.ventasdomiciliog1.R;
import com.nelo.misiontic2022.ventasdomiciliog1.mvp.LoginMVP;
import com.nelo.misiontic2022.ventasdomiciliog1.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private ImageView ivLogo;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;

    private AppCompatButton btnLogin;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnGoogle;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);

        initUI();
    }

    private void initUI() {
        ivLogo = findViewById(R.id.iv_logo);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(evt -> presenter.loginWithEmail());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(evt -> presenter.loginWithFacebook());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(evt -> presenter.loginWithGoogle());
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(etEmail.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilEmail.setError("");
        etEmail.setText("");
        tilPassword.setError("");
        etPassword.setText("");
    }

    @Override
    public void openPaymentsActivity() {
        Intent intent = new Intent(this, PaymentsActivity.class);
        startActivity(intent);
    }
}

