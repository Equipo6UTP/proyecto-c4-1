package com.nelo.misiontic2022.ventasdomiciliog1.presenter;

import com.nelo.misiontic2022.ventasdomiciliog1.model.LoginInteractor;
import com.nelo.misiontic2022.ventasdomiciliog1.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

        private LoginMVP.View view;
        private LoginMVP.Model model;

        public LoginPresenter(LoginMVP.View view) {
            this.view = view;
            this.model = new LoginInteractor();
        }

        @Override
        public void loginWithEmail() {
            boolean error = false;

            view.showEmailError("");
            view.showPasswordError("");

            LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
            if (loginInfo.getEmail().trim().isEmpty()) {
                view.showEmailError("Correo electrónico es obligatorio");
                error = true;
            } else if (!isEmailValid(loginInfo.getEmail().trim())) {
                view.showEmailError("Correo electrónico no es válido");
                error = true;
            }

            if (loginInfo.getPassword().trim().isEmpty()) {
                view.showPasswordError("Contraseña es obligatoria");
                error = true;
            } else if (!isPasswordValid(loginInfo.getPassword().trim())) {
                view.showPasswordError("Contraseña no cumple criterios de seguridad");
                error = true;
            }

            // TODO validar que el usuario/contraseña sean correctos

            if (!error) {
                view.openPaymentsActivity();
            } else {
                view.showGeneralError("Verifique los datos");
            }
        }

        private boolean isEmailValid(String email) {
            return email.contains("@")
                    && email.endsWith(".com");
        }

        private boolean isPasswordValid(String password) {
            return password.length() >= 8;
        }

        @Override
        public void loginWithFacebook() {

        }

        @Override
        public void loginWithGoogle() {

        }
    }

