package com.nelo.misiontic2022.ventasdomiciliog1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.nelo.misiontic2022.ventasdomiciliog1.R;

public class NewSaleActivity extends AppCompatActivity {

    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAddress;
    private TextInputEditText etAddress;
    private TextInputLayout tilAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etnumber;
    private TextInputLayout tilPeriodicity;
    private MaterialAutoCompleteTextView etPeriodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;
    private TextInputEditText etDate;
    private AppCompatButton btSave;

    private Date selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);

        initUI();
        loadData();
    }

    private void initUI() {

        etClient = findViewById(R.id.et_client);

        tilDate = findViewById(R.id.til_date);
        tilDate.setEndIconOnClickListener(v -> onDateClick());

        etDate = findViewById(R.id.et_date);
    }

    private void loadData(){
        etClient.setText(getIntent().getCharSequenceExtra("Client"));
    }

    private void onDateClick() {
        // Seleccion fecha actual
        if (selectedDate == null) {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cal.add(Calendar.DAY_OF_MONTH, 1);
            selectedDate = cal.getTime();
        }

        // Restricción de fechas (desde mañana)
        CalendarConstraints constraints = new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.now())
                .build();

        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder
                .datePicker()
                .setSelection(selectedDate.getTime())
                .setCalendarConstraints(constraints)
                .setTitleText(R.string.newsale_date)
                .build();
        datePicker.addOnPositiveButtonClickListener(this::onDateSelected);
        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void onDateSelected(Long selection) {
        selectedDate = new Date(selection);

        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }
}

