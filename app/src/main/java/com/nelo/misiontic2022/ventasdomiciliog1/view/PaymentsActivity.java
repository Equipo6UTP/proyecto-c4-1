package com.nelo.misiontic2022.ventasdomiciliog1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import com.nelo.misiontic2022.ventasdomiciliog1.R;

public class PaymentsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private MaterialToolbar appBar;
    private NavigationView navigationDrawer;

    private RecyclerView rvPayments;
    private FloatingActionButton btnSale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        initUI();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationitemSelected);

        rvPayments = findViewById(R.id.rv_payments);
        btnSale = findViewById(R.id.btn_sale);
        btnSale.setOnClickListener(v -> onNewSaleClick());
    }

    private void openDrawer() {
        drawerLayout.openDrawer(navigationDrawer);
    }

    private boolean navigationitemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }

    private void onNewSaleClick() {
        Intent intent = new Intent(PaymentsActivity.this, NewSaleActivity.class);
        intent.putExtra("Client", "Cesar Diaz");
        startActivity(intent);
    }

}

