package com.nelo.misiontic2022.ventasdomiciliog1.mvp;

public interface LoginMVP {

    interface Model {

    }

    interface Presenter {
        void loginWithEmail();
        void loginWithFacebook();
        void loginWithGoogle();
    }

    interface View {
        LoginInfo getLoginInfo();

        void showEmailError(String error);
        void showPasswordError(String error);
        void showGeneralError(String error);

        void clearData();

        void openPaymentsActivity();
    }

    class LoginInfo {
        private String email;
        private String password;

        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
